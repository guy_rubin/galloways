<?php

/* @material_admin/misc/notification-drawer.html.twig */
class __TwigTemplate_0405a9570040ac9427def722bd033eeadc01c26ed3b6c32997d4452e21a47043 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("trans" => 10, "if" => 16);
        $filters = array();
        $functions = array("url" => 16);

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('trans', 'if'),
                array(),
                array('url')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 8
        echo "<div id=\"messageContainer\" class=\"modal bottom-sheet\">
  <div class=\"modal-content\">
    <h4 class=\"notification-title\">";
        // line 10
        echo t("Message Notifications", array());
        echo "</h4>
    <div class=\"allmessages\">";
        // line 11
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "status", array()), "html", null, true));
        echo "
    </div>
  </div>
  <div class=\"modal-footer\">
    <a data-href=\"#!\" class=\"modal-action modal-close waves-effect waves-green btn-flat\">";
        // line 15
        echo t("Close", array());
        echo "</a>
   ";
        // line 16
        if (($context["dblog_link"] ?? null)) {
            echo " <a href=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("dblog.overview")));
            echo "\" class=\"modal-action modal-close waves-effect btn-flat\">";
            echo t("View DB Log", array());
            echo "</a> ";
        }
        // line 17
        echo "  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@material_admin/misc/notification-drawer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 17,  62 => 16,  58 => 15,  51 => 11,  47 => 10,  43 => 8,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@material_admin/misc/notification-drawer.html.twig", "/Users/mac2/Desktop/Workspace-2016/Galloways/galloways-site/backend/themes/material_admin 2/templates/misc/notification-drawer.html.twig");
    }
}
