<?php

/* modules/contrib/fiu/templates/fine-image-widget.html.twig */
class __TwigTemplate_dbcb37d9bbc693f13e3f63de71c1e55d8c42f31e38287a2c095fd1dda8f581b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 15, "for" => 22);
        $filters = array("without" => 22);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('if', 'for'),
                array('without'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 15
        if (($context["info"] ?? null)) {
            // line 16
            echo "  <div class=\"fiu-image-info\">
    <div class=\"fiu-full-image\">
      <div class=\"inner-fiu-full-image\">
        <img src=\"";
            // line 19
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute(($context["info"] ?? null), "url", array()), "value", array()), "html", null, true));
            echo "\">
        <div class=\"fiu-img-description\">
        <ul>
          ";
            // line 22
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_without(($context["info"] ?? null), "url"));
            foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
                // line 23
                echo "            ";
                if ($context["row"]) {
                    // line 24
                    echo "              <li>
                <b>";
                    // line 25
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($context["row"], "title", array()), "html", null, true));
                    echo " :</b>
                ";
                    // line 26
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($context["row"], "value", array()), "html", null, true));
                    echo "
              </li>
            ";
                }
                // line 29
                echo "          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 30
            echo "        </ul>
        <div class=\"attr\">
          ";
            // line 32
            if ($this->getAttribute(($context["data"] ?? null), "alt", array())) {
                // line 33
                echo "            ";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["data"] ?? null), "alt", array()), "html", null, true));
                echo "
          ";
            }
            // line 35
            echo "
          ";
            // line 36
            if ($this->getAttribute(($context["data"] ?? null), "title", array())) {
                // line 37
                echo "            ";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["data"] ?? null), "title", array()), "html", null, true));
                echo "
          ";
            }
            // line 39
            echo "        </div>
        </div>
      </div>
    </div>
  </div>
";
        }
        // line 45
        echo "
<div";
        // line 46
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["attributes"] ?? null), "html", null, true));
        echo ">
  ";
        // line 47
        if ($this->getAttribute(($context["data"] ?? null), "preview", array())) {
            // line 48
            echo "    <div class=\"fine-image-preview\">
      ";
            // line 49
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["data"] ?? null), "preview", array()), "html", null, true));
            echo "
    </div>
  ";
        }
        // line 52
        echo "
 ";
        // line 54
        echo "  <div class=\"fine-image-data\">
    <span class=\"fiu-image-details\">details</span>

    ";
        // line 57
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_without(($context["data"] ?? null), "preview", "title", "alt"), "html", null, true));
        echo "
  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "modules/contrib/fiu/templates/fine-image-widget.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 57,  134 => 54,  131 => 52,  125 => 49,  122 => 48,  120 => 47,  116 => 46,  113 => 45,  105 => 39,  99 => 37,  97 => 36,  94 => 35,  88 => 33,  86 => 32,  82 => 30,  76 => 29,  70 => 26,  66 => 25,  63 => 24,  60 => 23,  56 => 22,  50 => 19,  45 => 16,  43 => 15,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "modules/contrib/fiu/templates/fine-image-widget.html.twig", "/Users/mac2/Desktop/Workspace-2016/Galloways/galloways-site/backend/modules/contrib/fiu/templates/fine-image-widget.html.twig");
    }
}
