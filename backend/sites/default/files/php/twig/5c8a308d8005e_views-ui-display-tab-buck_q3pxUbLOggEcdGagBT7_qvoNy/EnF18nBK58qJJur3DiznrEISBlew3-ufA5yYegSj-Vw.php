<?php

/* themes/material_admin 2/templates/admin/views-ui-display-tab-bucket.html.twig */
class __TwigTemplate_1ec03e072eb3cba33e67ab0d3b2b03744dbf8d5ac7704b3bfbe5d77719b991df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 19, "if" => 28);
        $filters = array("clean_class" => 23, "t" => 31);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'if'),
                array('clean_class', 't'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 19
        $context["classes"] = array(0 => "views-ui-display-tab-bucket", 1 => "collection with-header", 2 => "z-depth-1", 3 => ((        // line 23
($context["name"] ?? null)) ? (\Drupal\Component\Utility\Html::getClass(($context["name"] ?? null))) : ("")), 4 => ((        // line 24
($context["overridden"] ?? null)) ? ("overridden") : ("")));
        // line 27
        echo "<div";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["attributes"] ?? null), "addClass", array(0 => ($context["classes"] ?? null)), "method"), "html", null, true));
        echo ">
  ";
        // line 28
        if (($context["title"] ?? null)) {
            // line 29
            echo "<h2 class=\"collection-header no-bottom-border views-ui-display-tab-bucket__title small-header grey lighten-4\">";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title"] ?? null), "html", null, true));
            echo "</h3>
  ";
        } elseif ($this->getAttribute(        // line 30
($context["attributes"] ?? null), "hasClass", array(0 => "access"), "method")) {
            // line 31
            echo "    <h2 class=\"collection-header no-bottom-border views-ui-display-tab-bucket__title small-header grey lighten-4\">";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Access")));
            echo "</h3>";
        }
        // line 33
        echo "  ";
        if (($context["actions"] ?? null)) {
            // line 34
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["actions"] ?? null), "html", null, true));
        }
        // line 36
        echo "  ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["content"] ?? null), "html", null, true));
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "themes/material_admin 2/templates/admin/views-ui-display-tab-bucket.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 36,  69 => 34,  66 => 33,  61 => 31,  59 => 30,  54 => 29,  52 => 28,  47 => 27,  45 => 24,  44 => 23,  43 => 19,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/material_admin 2/templates/admin/views-ui-display-tab-bucket.html.twig", "/Users/mac2/Desktop/Workspace-2016/Galloways/galloways-site/backend/themes/material_admin 2/templates/admin/views-ui-display-tab-bucket.html.twig");
    }
}
