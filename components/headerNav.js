import { Container, Row, Col, Nav, NavItem, Form, NavLink, Collapse,
    Navbar, NavbarToggler, NavbarBrand, Modal, ModalHeader, ModalBody,
    ModalFooter, ListGroup, ListGroupItem } from 'reactstrap'
import Link from 'next/link'
import Fade from 'react-reveal/Fade';
import Slide from 'react-reveal/Slide';

const HeaderNav = (props) => 
    <Navbar light className="navbar navbar-expand-md">
        <div className={'menu--holder ' + props.dark } >
        <Container className="wide d-flex">
            
            <Col xs="12" md="5">
                <Slide down>
                    <Link prefetch href="/"> 
                        <NavbarBrand href="/">
                            {(() => {
                                switch (props.dark) {
                                  case "menu--dark":   return <img src="/static/assets/img/layout/logo-dark.png" />
                                  case "menu--darkBlue": return <img src="/static/assets/img/layout/logo.png" />
                                  default: return <img src="/static/assets/img/layout/logo.png" />
                                }
                            })()}
                        </NavbarBrand>
                    </Link>
                </Slide>
            </Col>
            
            
            <Col xs="12" md="7" className="align-self-end">
             <Fade down>
                <div className="nojs-navbar">
                <Nav navbar className="row">
                <div className={'menu menu--top ' + props.topMenu }>
                    <Link prefetch href="/news">
                    <a href="/news">News</a>
                    </Link>
                    <Link prefetch href="/contact">
                    <a href="/contact">Contact</a>
                    </Link>
                    <Link prefetch href="/examples/routing">
                    <a href="/examples/routing">Client Login</a>
                    </Link>
                    <div className="social--links">
                    <Link prefetch href="/examples/styling">
                        <a href="/examples/styling"><i className="fab fa-twitter"></i></a>
                    </Link>
                    <Link prefetch href="/examples/styling">
                        <a href="/examples/styling"><i className="fab fa-linkedin"></i></a>
                    </Link>
                    </div>
                </div>
                <div tabIndex="1">
                    <div className="menu">
                    <Link prefetch href="/about">
                        <a href="/about">Who we are</a>
                    </Link>
                    <Link prefetch href="/what-we-do">
                        <a href="/what-we-do">What we do</a>
                    </Link>
                    <Link prefetch href="/we-work-with">
                        <a href="/we-work-with">Who we work with</a>
                    </Link>
                    <Link prefetch href="/work-with-us">
                        <a href="/work-with-us">Work with us</a> 
                    </Link>
                    </div>
                </div>
                </Nav>
            </div>
            </Fade>
            </Col>
           
        </Container>
        </div>
    </Navbar>

    export default HeaderNav



