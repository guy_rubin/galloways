import { Container } from 'reactstrap'
const Intro = (props) =>
    <div className="header--content"> 
        <Container>
            { props.content }
        </Container>    
    </div>


    export default Intro
