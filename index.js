'use strict'

const next = require('next')
const nextAuth = require('next-auth')
const nextAuthConfig = require('./next-auth.config')

var cors = require('cors')
const routes = {
  admin:  require('./routes/admin'),
  account:  require('./routes/account')
}
const LRUCache = require('lru-cache');

const ssrCache = new LRUCache({
   max: 100,
   maxAge: 1000 * 60 * 60 // 1hour
});

// Load environment variables from .env file if present
require('dotenv').load()

process.on('uncaughtException', function(err) {
  console.error('Uncaught Exception: ', err)
})

process.on('unhandledRejection', (reason, p) => {
  console.error('Unhandled Rejection: Promise:', p, 'Reason:', reason)
})

// Default when run with `npm start` is 'production' and default port is '80'
// `npm run dev` defaults mode to 'development' & port to '3000'
process.env.NODE_ENV = process.env.NODE_ENV || 'production'
process.env.PORT = process.env.PORT || 8000

// Initialize Next.js
const nextApp = next({
  dir: '.',
  dev: (process.env.NODE_ENV === 'development')
})

// Add next-auth to next app
nextApp
.prepare()
.then(() => {
  // Load configuration and return config object
  return nextAuthConfig()
  function getCacheKey(req){
    //TODO clean-up, standardize an url to maximize cache hits
    return req.url
   }
   
   async function renderAndCache(req, res, pagePath, queryParams) {
     //TODO add a way to purge cache for a specific url
     const key = getCacheKey(req);
   
     // If we have a page in the cache, let's serve it
     if (ssrCache.has(key)) {
       res.setHeader('x-cache', 'HIT');
       res.send(ssrCache.get(key));
       return;
     }
     
     // No cache present for specific key? let's try to render and cache
    try {
      const html = await app.renderToHTML(req, res, pagePath, queryParams);
      // If something is wrong with the request, let's not cache
      // Send the generated content as is for further inspection
   
      if (dev || res.statusCode !== 200) {
       res.setHeader('x-cache', 'SKIP');
       res.send(html);
       return;
       }
   
       // Everything seems OK... let's cache
       ssrCache.set(key, html);
       res.setHeader('x-cache', 'MISS');
       res.send(html);
     } catch (err) {
       app.renderError(err, req, res, pagePath, queryParams);
    }
  }
})
.then(nextAuthOptions => {
  // Pass Next.js App instance and NextAuth options to NextAuth
  // Note We do not pass a port in nextAuthOptions, because we want to add some
  // additional routes before Express starts (if you do pass a port, NextAuth
  // tells NextApp to handle default routing and starts Express automatically).
  return nextAuth(nextApp, nextAuthOptions)
})
.then(nextAuthOptions => {
  // Get Express and instance of Express from NextAuth
  const express = nextAuthOptions.express
  const expressApp = nextAuthOptions.expressApp

  // Add admin routes
  routes.admin(expressApp)
  
  // Add account management route - reuses functions defined for NextAuth
  routes.account(expressApp, nextAuthOptions.functions)
  
  // Serve fonts from ionicon npm module
  expressApp.use('/fonts/ionicons', express.static('./node_modules/ionicons/dist/fonts'))
  
  // A simple example of custom routing
  // Send requests for '/custom-route/{anything}' to 'pages/examples/routing.js'
  expressApp.get('/custom-route/:id', (req, res) => {
    // Note: To make capturing a slug easier when rendering both client
    // and server side, name it ':id'
    return nextApp.render(req, res, '/examples/routing', req.params)
  })
  
  expressApp.get('/news/:id', (req, res) => {
    const actualPage = '/news-story'
    const queryParams = { id: req.params.id } 
    console.log(req.params.id )
    nextApp.render(req, res, actualPage, queryParams)
  })

  // Default catch-all handler to allow Next.js to handle all other routes
  expressApp.all('*', (req, res) => {
    let nextRequestHandler = nextApp.getRequestHandler()
    return nextRequestHandler(req, res)
  })

  expressApp.listen(process.env.PORT, err => {
    if (err) {
      throw err
    }
    console.log('> Ready on http://localhost:' + process.env.PORT + ' [' + process.env.NODE_ENV + ']')
  })
})
.catch(err => {
  console.log('An error occurred, unable to start the server')
  console.log(err)
})


function getCacheKey(req){
  //TODO clean-up, standardize an url to maximize cache hits
  return req.url
 }
 
 async function renderAndCache(req, res, pagePath, queryParams) {
   //TODO add a way to purge cache for a specific url
   const key = getCacheKey(req);
 
   // If we have a page in the cache, let's serve it
   if (ssrCache.has(key)) {
     res.setHeader('x-cache', 'HIT');
     res.send(ssrCache.get(key));
     return;
   }
   
   // No cache present for specific key? let's try to render and cache
  try {
    const html = await app.renderToHTML(req, res, pagePath, queryParams);
    // If something is wrong with the request, let's not cache
    // Send the generated content as is for further inspection
 
    if (dev || res.statusCode !== 200) {
     res.setHeader('x-cache', 'SKIP');
     res.send(html);
     return;
     }
 
     // Everything seems OK... let's cache
     ssrCache.set(key, html);
     res.setHeader('x-cache', 'MISS');
     res.send(html);
   } catch (err) {
     app.renderError(err, req, res, pagePath, queryParams);
  }
 }