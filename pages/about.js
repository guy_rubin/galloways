import React from 'react'
import Link from 'next/link'
import { Container, Row, Col, Jumbotron, ListGroup, ListGroupItem } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'
import '../components/parallax'
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Icon from '@material-ui/core/Icon'
import $ from "jquery";
import Fuse from "fuse.js";
import '../components/team';
import Fade from 'react-reveal/Fade';
import Slide from 'react-reveal/Slide';
import renderHTML from 'react-render-html';

export default class extends Page {
  constructor(props) {
    super(props)
    this.state = {
      members: [],
      filteredMembers: [],
      searchName: '',
      visible: 10,
      about_data: [],
      error: false
    };
    this.loadMore = this.loadMore.bind(this);
  }

  loadMore() {
    this.setState((prev) => {
      return {visible: prev.visible + 5};
    });
  }

  onChange(event) {

    this.setState({searchName: event.target.value});

  }

  
  componentWillMount(){
    fetch('http://localhost/about', {mode: 'cors'})
    .then(res => res.json())
    .then(json => {
        this.setState({
          about_data: json,
        })
    })
    fetch('http://localhost/team', {mode: 'cors'})
    .then(res => res.json())
    .then(json => {
        this.setState({
          members: json,
          filteredMembers: json
        })
    })
    if (typeof window !== 'undefined') {


        document.body.className = "about bg-yellow";
        var s = skrollr.init();
        $(document).ready(function() {
          s.refresh();
          $('.mtt-member--popup').hide()
          $( ".mtt-members" ).on("click", ".mtt-member", function() {
            console.log('text');
            $('.mtt-member--popup-name').text($(this).attr('data-member-name'));
            $('.mtt-member--popup-surname').text($(this).attr('data-member-surname'));
            $('.mtt-member--popup-title').text($(this).attr('data-member-title'));
            $('.mtt-member--popup-description').html($(this).attr('data-member-description'));
            $('.mtt-member--popup-email span').text($(this).attr('data-member-email'));
            $('.mtt-member--popup-number span').text($(this).attr('data-member-number'));

            $('.mtt-member').fadeOut()
            $('.mtt-load button').fadeOut()
            $('.mtt-member--popup').fadeIn()
            $(this).fadeIn()
            $( ".mtt-members").addClass('active')
          });
          $( ".mtt-members" ).on("click", ".mtt-member--popup-close", function() {
            console.log('text');
            $('.mtt-member').fadeIn()
            $('.mtt-load button').fadeIn()
            $('.mtt-member--popup').fadeOut()
            $( ".mtt-members").removeClass('active')
          });
      });
    }
  }
  componentDidUpdate() {
    var s = skrollr.init();
    s.refresh();
    console.log('update');
  }
  componentWillUnmount(){
    if (typeof window !== 'undefined') {
    }
}

  render() {
    const introCont = (
      <div className="header--intro">
        <h1 className="intro--text">Statement about how Galloways make their clients grow.</h1>
        <Link prefetch href="/about">
          <Button href="/about"  variant="outlined" className="btn-large darkBlue" size="large"> CONTACT US <i className="material-icons"> keyboard_arrow_right </i></Button>
        </Link>
        <div className="intro--img" ></div>
      </div>)
    const overlay = ''
    const data = this.state.about_data
    console.log(this.state.searchName)
    return (
      <Layout {...this.props} navmenu={false} container={false} content={introCont} overlay={overlay} darkMenu={'menu--dark'} topMenu={'txt_white'}>
      <div>
      { data.map((about, index) => { 
        return (
          
       <section className="bg-darkBlue">
          <Container fluid>
          <Fade up> 
          <div className="row d-flex">
          <Col xs="12" lg="2" md={{ size: '2', offset: 2 }} className="align-self-center txt_white">
            <h2>
              <div className="txt_green">
                { about.field_section1_title[0].value }
              </div>
            </h2>
            { renderHTML(about.field_section1_text[0].value) }
          </Col>
          <Col xs="12" lg="6" md={{ size: '6', offset: 1 }} className="d-flex">
            <div className="Gridbox services bg-green" data-0="left: -100px; top: -40px;" data-2000="left: 20px; top: 80px;">
              <Row>
                <Col xs="12" lg="6" md={{ size: '6' }}>
                  <div className="services--title txt_white">
                    { about.field_service_1_title[0].value }
                  </div>
                  <div className="services--text txt_white">
                    { renderHTML(about.field_service_1_description[0].processed) }
                  </div>
                </Col>
                <Col xs="12" lg="6" md={{ size: '6' }}>
                  <div className="services--title txt_white">
                    { about.field_service_2_title[0].value }
                  </div>
                  <div className="services--text txt_white">
                    { renderHTML(about.field_service_2_description[0].processed) }
                  </div>
                </Col>
              </Row>
            </div>
          </Col>
        </div>
        </Fade> 
        </Container>
        <div className="section--overlay bg-white" data-0="width: 55%;" data-2000="width: 60%;"></div>
       </section>
       );
      })}
       <section className="mtt bg-white">
          <Container>

            <div>
              <Row className=" d-flex justify-content-center">
                <Col xs="12" lg="5" className="align-self-center">
                <Fade up> 
                <div className="align-self-center">
                  <h2>
                    <div className="darkBlue align-self-center" >
                      Meet the team
                    </div>
                  </h2>
                  <p>Everybody in our firm works with clients, so we make sure you can find everybody in our firm on our websit</p>
                  <div className="mtt-search">
                  <div className="">
                    <Grid container spacing={8} alignItems="flex-end" className="hidden">
                      <Grid item>
                        <TextField id="input-with-icon-grid" label="Looking for someone..." />
                      </Grid>
                      <Grid item>
                        <Icon> search </Icon>
                      </Grid>
                    </Grid>
                    <div className="mtt--search-box">
                      <TextField 
                        id="outlined-textarea"
                        label="Looking for someone..."
                        placeholder="Enter name"
                        multiline
                        className="mtt--search-box-input"
                        margin="normal"
                        variant="outlined"
                        onKeyPress={this.onChange.bind(this)}
                        onChange={this.onChange.bind(this)}
                      />
                      <Icon> search </Icon>
                    </div>
                  </div>
                  </div>
                  </div>
                  </Fade> 
                </Col>
              </Row>
              
              <Row>
              <Col xs="12" lg="12" className="mtt-members align-self-center">
                <Row>
                  <div className="mtt-members-holder">
                    {this.state.filteredMembers.slice(0, this.state.visible).map((item, index) => {
                        return (
                          <Fade>
                            <Col className="mtt-member"
                              data-member-id={ item.nid[0].value }
                              data-member-name={ item.field_name[0].value }
                              data-member-surname={ item.field_surname[0].value }
                              data-member-title={ item.field_job_title[0].value }
                              data-member-description={ item.field_description[0].value }
                              data-member-email={ item.field_email[0].value }
                              data-member-number={ item.field_phone_number[0].value }
                              >
                                <img src={ item.field_image[0].url } />
                                <div className="mtt-member--overlay">
                                    <div className="mtt-member--overlay-name">{ item.field_name[0].value }</div>
                                </div>
                            </Col>
                          </Fade>
                        );
                    })}
                    </div>
                  
                  </Row>
                  <div className="mtt-member--popup">
                  <div className="mtt-member--popup-close"><i className="material-icons">
                  close
                  </i></div>
                  <div className="mtt-member--popup-name">
                    Name
                  </div>
                  <div className="mtt-member--popup-surname">
                    Surname
                  </div>
                  <div className="mtt-member--popup-title">
                    Title
                  </div>
                  <div className="mtt-member--popup-description">
                    Description...
                  </div>
                  <div className="mtt-member--popup-email">
                    <img src="/static/assets/img/icons/mtt-mail.png" />
                    <span>Email</span>
                  </div>
                  <div className="mtt-member--popup-number">
                    <img src="/static/assets/img/icons/mtt-phone.png" />
                    <span>Number</span>
                  </div>
                </div>
                </Col>
                {this.state.visible < this.state.members.length && (
                  <div className="d-flex justify-content-center mtt-load">
                      <Button onClick={this.loadMore} variant="outlined" className="btn-large darkBlue" size="large"> LOAD MORE </Button>
                  </div>
                )}
                
                </Row>
              
            </div>
            
         </Container>
       </section>
       <section className="bg-lightBlue vacancies-footer">
        <Container>
        <Fade up> 
          <div className="row d-flex">
            <Col xs="12" md={{ size: '6', offset: '1  ' }} lg="6">
            </Col>
            <Col xs="12" md="4" lg="4" className="align-self-center txt_white">
              <h2>
                <div>
                  We’re hiring
                </div>
              </h2>
              <p>
                We are always looking for great people, at all levels from partner to trainee, to join our team.
              </p>
              <Link prefetch href="/about">
                <Button href="/"  variant="outlined" className="btn-large darkBlue" size="large"> VIEW CURRENT VACANCIES <i className="material-icons"> keyboard_arrow_right </i></Button>
              </Link>
            </Col>
          </div>
          </Fade> 
        </Container>
       </section>
       </div>
      </Layout>
    )
  }
}
