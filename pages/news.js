import React from 'react'
import Link from 'next/link'
import { Container, Row, Col, Jumbotron, ListGroup, ListGroupItem } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'
import '../components/parallax'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Icon from '@material-ui/core/Icon'
import $ from "jquery";
import '../components/team';
import Fade from 'react-reveal/Fade';
import Slide from 'react-reveal/Slide';


export default class extends Page {
  constructor(props) {
    super(props)
    this.state = {
        news: [],
        isLoaded: false
    };
  }

  componentDidMount(){
    fetch('http://localhost/news', {mode: 'cors'})
        .then(res => res.json())
        .then(json => {
            this.setState({
                isLoaded: true,
                news: json,
            })
        })
       

    if (typeof window !== 'undefined') {

        
        document.body.className = "news bg-darkBlue";
        var s = skrollr.init();
        $(document).ready(function() {
          s.refresh();
          console.log('Loaded');
      });
    }
  }
  componentDidUpdate() {
    var s = skrollr.init();
    s.refresh();
    console.log('update');
  }
  componentWillUnmount(){
    if (typeof window !== 'undefined') {
    }
}

  render() {
    const { isLoaded, news } = this.state
    //console.log(this.state.news)

    const posts = this.state.news; 
    const blog = this.state.news[0]
    console.log(this.state.news[0]);
    
    const introCont = (
    <div className="header--intro">
        <h1 className="intro--text">Statement about all latest news goes here</h1>
        <div className="intro--img" ></div>
      </div>)
    const overlay = ''

        return (
        <Layout {...this.props} navmenu={false} container={false} content={introCont} overlay={overlay} darkMenu={'menu--darkBlue'} topMenu={'txt_green'}>

            <section className="bg-white">
                <Container className="wide">
                    <div className="news-posts">
                    { posts.map((post, index) => {
                        return (
                            <Fade up>
                                <div className="news-post" key="${index}">
                                    <div className="news-post--image" style={{ backgroundImage: 'url('+ post.field_image[0].url+')' }}></div>
                                    <div className="news-post--content">
                                        <div className="news-post--content-date">28 FEB 2019</div>
                                        <div className="news-post--content-title">
                                        { post.title[0].value }
                                        </div>
                                        <div className="news-post--content-snippet">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore...</div>
                                        <div className="news-post--content-link"><Link as={`/news/${post.title[0].value.replace(/\s+/g, '-').toLowerCase()}`} href={"/news-story?id="+ post.title[0].value.replace(/\s+/g, '-').toLowerCase() +""}><a>READ MORE <i className="material-icons"> keyboard_arrow_right </i></a></Link></div>
                                    </div>
                                </div>
                            </Fade>
                            );
                    })}
                    </div>
                    <Row className="d-flex justify-content-center pt-5 pagination">
                        <a href=""><i className="material-icons"> keyboard_arrow_left </i> SEE OLDER STORIES </a>
                        <a href="">SEE NEWER STORIES <i className="material-icons"> keyboard_arrow_right </i></a>
                    </Row>

                </Container>
            </section>
        </Layout>
        )
    }
}