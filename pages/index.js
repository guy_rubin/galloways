import Link from 'next/link'
import React from 'react'
import { Container, Row, Col, Jumbotron, ListGroup, ListGroupItem } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'
import '../components/parallax'
import Button from '@material-ui/core/Button';
import $ from "jquery";
import Fade from 'react-reveal/Fade';
import Slide from 'react-reveal/Slide';
import renderHTML from 'react-render-html';


export default class extends Page {
  constructor(props) {
    super(props)
    this.state = {
      home_slides: [],
      home_data: [],
      ourClients: [],
      services: [],
      visible: 3,
      error: false
      }
      this.loadMore = this.loadMore.bind(this);
  }
 

  loadMore() {
    this.setState((prev) => {
      return {visible: prev.visible + 3};
    });
  }
  componentWillMount(){

    fetch('http://localhost/clients', {mode: 'cors'})
    .then(res => res.json())
    .then(json => {
        this.setState({
          ourClients: json,
        })
    })
    fetch('http://localhost/home-services', {mode: 'cors'})
    .then(res => res.json())
    .then(json => {
        this.setState({
          services: json,
        })
    })
    fetch('http://localhost/home-slides', {mode: 'cors'})
        .then(res => res.json())
        .then(json => {
            this.setState({
                home_slides: json,
            })
            if (typeof window !== 'undefined') {
            $(document).ready(function() {
              var $ = require('jquery');
              window.jQuery = $;
              require('../node_modules/slick-carousel/slick/slick.js');
              $('.intro--slider').slick({
                prevArrow: '<button class="slick-prev slick-arrow" aria-label="Previous" type="button" style="display: block;"><i class="material-icons">chevron_left</i></button> ',
                nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button" style="display: block;"><i class="material-icons">chevron_right</i></button>',
                responsive: [
                  {
                    breakpoint: 991,
                    settings: {
                      dots: true
                    }
                  }
                ]
              }); /* Initialize the slick again */
            });
          }
        })
      fetch('http://localhost/home', {mode: 'cors'})
        .then(res => res.json())
        .then(json => {
            this.setState({
              home_data: json,
            })
        })
      

    
    if (typeof window !== 'undefined') {
        document.body.className = "home";
        console.log('in');
        var s = skrollr.init();
        $(document).ready(function() {
          s.refresh();

          console.log('Loaded');
      });
    }
  }
componentDidUpdate() {
    var s = skrollr.init();
    s.refresh();
    console.log('update = ' + this.state.storyId);
  }
  componentWillUnmount(){
    if (typeof window !== 'undefined') {
      skrollr.init().destroy();
      var s = skrollr.init();
      s.refresh();
      console.log('out');
    }
}

  render() {
    const slides = this.state.home_slides
    const data = this.state.home_data
    const ourClients = this.state.ourClients
    const services = this.state.services
    console.log(ourClients)
    const introCont =
      <div className="intro--slider">
      { slides.map((slide, index) => {
        return (
            <Fade delay={500}>
              <div className="intro--slide" data-key={index}>
                  <div className="header--intro">
                    <h1 className="intro--text">{ slide.field_quote[0].value }</h1>
                    <div className="intro--author">{ slide.field_author[0].value }</div>
                    <div className="intro--img" ><img src={ slide.field_picture[0].url } />
                  </div>
                </div>
              </div>
            </Fade>
        );
    })}
    </div>
  
    const overlay = 'bg-orange'
    return (
      <Layout {...this.props} navmenu={false} container={false} content={introCont} overlay={overlay} darkMenu={''}>
      { data.map((home, index) => { 
        return (
        <div>
        <section>
            <Container>
              <div className="row d-flex">
                <Col xs="12" md="5" lg="5" className="align-self-center">
                <Row>
                  <Fade left>
                  <h2>
                    <div className="txt_green">
                      { home.field_section1_title[0].value }
                    </div>
                  </h2>
                   { renderHTML(home.field_section1_text[0].value) }

                  <Button href={home.field_section1_button_link[0].uri.replace('internal:', '').replace('external:', '')}  variant="outlined" className="btn-large darkBlue" size="large"> { home.field_section1_button_link[0].title } <i className="material-icons"> keyboard_arrow_right </i></Button>
                  </Fade>
                  </Row>
                </Col>
                <Col xs="12" md="7" lg="7" className="d-flex justify-content-center">
                <Row>
                  <Fade right>
                    <img src="https://via.placeholder.com/470x660" />
                  </Fade>
                  </Row>
                </Col>
              </div>
          </Container>
        </section>
        <section className="bg-darkBlue">
            <Container fluid>
            <Fade up>
            <div className="row d-flex">
            <Col xs="12" md="12" lg={{ size: '3', offset: 1 }} xl={{ size: '2', offset: 2 }} className="align-self-center txt_white">
              <h2>
                <div className="txt_green">
                { home.field_section2_title[0].value }
                </div>
              </h2>
              { renderHTML(home.field_section2_text_[0].value) }

            </Col>
            <Col xs="12" lg="6" md={{ size: '6', offset: 1 }} className="d-flex">
              <div className="Gridbox services" data-0="left: -140px; top: -80px;" data-2000="left: -40px; top: 80px;">
             
                  <Row>
                  { services.map((service, index) => { 
                    return (
                    <Col key={index} xs="12" lg="6" md={{ size: '6' }}>
                      <div className="services--icon">
                        <img src="/static/assets/img/home/service1.png" />
                      </div>
                      <div className="services--title">
                        { service.title[0].value }
                      </div>
                      <div className="services--text">
                        { renderHTML(service.field_service_description[0].value) }
                      </div>
                    </Col>
                        );
                    })}
                  </Row>
                  
              </div>
            </Col>
          </div>
          </Fade>
          </Container>
            <div className="section--overlay bg-yellow" data-0="width: 55%;" data-2000="width: 60%;"></div>
        </section>
        
        <section className="work-with">
            <Container fluid>
              <Fade up>
              <div className="row d-flex">
                <Col xs="12" lg="2" md={{ size: '2', offset: 3 }} className="align-self-center p-0">
                  <Row><h2>
                    <div className="txt_green">
                      { home.field_section3_title[0].value }
                    </div>
                  </h2>
                  { renderHTML(home.field_section3_text_[0].value) }
                  </Row>
                </Col>
                <Col xs="12" lg="5" md={{ size: '5', offset: 1 }} className="work-with--clients align-self-center">
                  <Row>
                  {this.state.ourClients.slice(0, this.state.visible).map((client, index) => {
                      
                    return (
                      
                        <Col key={index} xs="12" md="4"><Fade up><img src={ client.field_client_logo[0].url } /></Fade></Col>
                     );
                    })}
                    
                  </Row>
                  {this.state.visible < this.state.ourClients.length && (
                    <div className="d-flex justify-content-center mtt-load">
                        <Button onClick={this.loadMore} variant="outlined" className="btn-large darkBlue" size="large"> LOAD MORE </Button>
                    </div>
                  )}
                </Col>
              </div>
              </Fade> 
          </Container>
        </section>
        <section className="bg-lightBlue locations">
          <Container>
            <Fade up>
            <div className="row d-flex">
              <Col xs="12" md="5" lg="5" className="align-self-center">
                <h2>
                  <div className="txt_white">
                    Get in touch<br /> with us
                  </div>
                </h2>
                <p>
                  <a href="mailto:hello@wearegalloways.com" className="locations--email underline--link ">hello@wearegalloways.com</a>
                </p>
              </Col>
              <Col xs="12" md={{ size: '6', offset: '1' }} lg="6">
                <Row>
                  <Col xs="12" md={{ size: '5', offset: '2' }} className="location">
                    <div className="location--title">Brighton</div>
                    <div className="location--details">
                      30 New Road<br />
                      Brighton<br />
                      East Sussex<br />
                      BN1 1BN<br />
                    </div>

                    <div className="location--tel align-self-end">
                      <a href="tel:01273 324163"><span>T: </span>01273 324163</a>
                    </div>
                  </Col>
                  <Col className="location">
                    <div className="location--title">Haywards Heath</div>
                    <div className="location--details">
                      Haywards Heath<br />
                      Boltro Road<br />
                      Haywards Heath<br />
                      Boltro Road<br />
                      BN1 1BN<br />
                    </div>
                    <div className="location--tel align-self-end">
                      <a href="tel:01444 416416 "><span>T: </span>01444 416416 </a>
                    </div>
                  </Col>
                </Row>
              </Col>
            </div>
            </Fade>
          </Container>
        </section>
        </div>
        );
      })}
      </Layout>
    )
  }
}

export class Intro extends React.Component {
  render() {
  }
}
