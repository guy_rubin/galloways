import React from 'react'
import Link from 'next/link'
import { Container, Row, Col, Jumbotron, ListGroup, ListGroupItem } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'
import '../components/parallax'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Icon from '@material-ui/core/Icon'
import $ from "jquery";
import '../components/team';
import Fade from 'react-reveal/Fade';
import Slide from 'react-reveal/Slide';
import shouldPureComponentUpdate from 'react-pure-render-utils/function';
import GoogleMap from 'google-map-react';
import renderHTML from 'react-render-html';

export default class extends Page {
  constructor(props) {
    super(props)
    this.state = {
        center: [50.904180, -0.004720],
        zoom: 11,
        greatPlaceCoords: {lat: 59.724465, lng: 30.080121}, 
        page_data: [],
    };
  }
  shouldComponentUpdate = shouldPureComponentUpdate;

  componentDidMount(){
    fetch('http://localhost/contact', {mode: 'cors'})
    .then(res => res.json())
    .then(json => {
        this.setState({
          page_data: json,
        })
    })
    if (typeof window !== 'undefined') {

        
        document.body.className = "contact bg-darkBlue";
        var s = skrollr.init();
        $(document).ready(function() {
          s.refresh();
          console.log('Loaded');
      });
    }
  }
  componentDidUpdate() {
    var s = skrollr.init();
    s.refresh();
    console.log('update');
  }
  componentWillUnmount(){
    if (typeof window !== 'undefined') {
    }
}

  render() {
    const introCont = (
      <div className="header--intro_map">
      </div>)
    const overlay = '';
    const data = this.state.page_data;
    const mapStyle = [ { "elementType": "geometry", "stylers": [ { "color": "#e7e7e7" } ] }, { "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] }, { "elementType": "labels.text.fill", "stylers": [ { "color": "#616161" } ] }, { "elementType": "labels.text.stroke", "stylers": [ { "color": "#e7e7e7" } ] }, { "featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [ { "color": "#bdbdbd" } ] }, { "featureType": "poi", "elementType": "geometry", "stylers": [ { "color": "#eeeeee" } ] }, { "featureType": "poi", "elementType": "labels.text.fill", "stylers": [ { "color": "#757575" } ] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [ { "color": "#d0d0d0" } ] }, { "featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [ { "color": "#9e9e9e" } ] }, { "featureType": "road", "elementType": "geometry", "stylers": [ { "color": "#ffffff" } ] }, { "featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [ { "color": "#757575" } ] }, { "featureType": "road.highway", "elementType": "geometry", "stylers": [ { "color": "#dadada" } ] }, { "featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [ { "color": "#616161" } ] }, { "featureType": "road.local", "elementType": "labels.text.fill", "stylers": [ { "color": "#9e9e9e" } ] }, { "featureType": "transit.line", "elementType": "geometry", "stylers": [ { "color": "#d0d0d0" } ] }, { "featureType": "transit.station", "elementType": "geometry", "stylers": [ { "color": "#eeeeee" } ] }, { "featureType": "water", "elementType": "geometry", "stylers": [ { "color": "#c9c9c9" } ] }, { "featureType": "water", "elementType": "labels.text.fill", "stylers": [ { "color": "#9e9e9e" } ] } ];

    console.log(mapStyle)
    
    return (
      <Layout {...this.props} navmenu={false} container={false} content={introCont} overlay={overlay} darkMenu={'menu--darkBlue'} topMenu={'txt_green'}>
      { data.map((contact, index) => { 
        return (
        <div>
        <div className="map-molder">
            <GoogleMap
            // apiKey={YOUR_GOOGLE_MAP_API_KEY} // set if you need stats etc ...
            bootstrapURLKeys={{ key: 'AIzaSyCdKS4_ohbsHqwpG4CLvFXplhmRQjixU4M' }}
            defaultOptions={{ styles: mapStyle }}
            options={{ styles: mapStyle }}
            defaultMapTypeId={'custom_style'}
            center={this.state.center}
            zoom={this.state.zoom}>
                <Button
                lat={50.823440}
                lng={-0.139465}
                className="map--marker"><img src="/static/assets/img/icons/marker.png" /></Button>
                <Button
                lat={50.997382}
                lng={-0.095887}
                className="map--marker"><img src="/static/assets/img/icons/marker.png" /></Button>
            </GoogleMap>
        </div>

        <section className="bg-white">
        <div className="map--contact-box">
            <div className="map--contact-box-location">Brighton</div> 
            <div className="map--contact-box-address">
            <p>
                30 New Rd<br />
                Brighton<br />
                East Sussex<br />
                BN1 1BN<br />
            </p>
            </div> 
            <div className="map--contact-box-number"><img src="/static/assets/img/icons/contact-number.png" />  <a className="underline--link" href="tel:01403 786788">01403 786788</a></div> 
            <div className="map--contact-box-fax"><img src="/static/assets/img/icons/contact-fax.png" /> <a className="underline--link" href="tel:01403 786780">01403 786780 </a></div> 
            <div className="map--contact-box-email"><img src="/static/assets/img/icons/contact-email.png" /><a className="underline--link" href="mailto:brighton@wearegalloways.com">brighton@wearegalloways.com</a></div> 
        </div>
            <Container>
                <div className="contact--form">
                    <Col xs="" md={{ size: '6', offset: 3 }}>
                        <Row className="d-flex justify-content-start">
                        <h2>
                            <div className="txt_green">
                                { contact.field_section1_title[0].value }
                            </div>
                        </h2>
                        </Row>
                        <Row>
                        <Col xs="12" className="p-0">
                            <div className="clients--smallText">
                            { renderHTML(contact.field_section1_text[0].value) }
                            </div>
                        </Col>
                    </Row>
                    <form>
                        <Row>
                        <TextField
                            id="outlined-with-placeholder"
                            label="Your name"
                            placeholder="Enter name"
                            className="MuiFormControl-root-108"
                            margin="normal"
                            variant="outlined"
                        />
                        </Row>
                        <Row>
                        <TextField
                            id="outlined-with-placeholder"
                            label="Your email address"
                            placeholder="Enter email"
                            className="MuiFormControl-root-108"
                            margin="normal"
                            variant="outlined"
                        />
                        </Row>
                        <Row>
                        <TextField
                            id="outlined-with-placeholder"
                            label="Your telephone number"
                            placeholder="Enter phone number"
                            className="MuiFormControl-root-108"
                            margin="normal"
                            variant="outlined"
                        />
                        </Row>
                        <Row>
                        <TextField
                            id="outlined-textarea"
                            label="Your message"
                            placeholder="Enter message"
                            multiline
                            className="MuiFormControl-root-108"
                            margin="normal"
                            variant="outlined"
                        />
                        </Row>
                        <Row className="pt-5 d-flex justify-content-center">
                        <Button variant="outlined" className="btn-large darkBlue" size="large" color="#1a5585"> SUBMIT <i className="material-icons"> keyboard_arrow_right </i></Button>
                        </Row>
                    </form>
                    </Col>
                </div>
            </Container>
        </section>
        </div>
        );
        })}
      </Layout>
    )
  }
}
