import React from 'react'
import Link from 'next/link'
import { Container, Row, Col, Jumbotron, ListGroup, ListGroupItem } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'
import '../components/parallax'
import Button from '@material-ui/core/Button';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import $ from "jquery";
import '../components/team';
import Fade from 'react-reveal/Fade';
import Slide from 'react-reveal/Slide';
import renderHTML from 'react-render-html';

export default class extends Page {
  constructor(props) {
    super(props)
    this.state = {
      page_data: [],
      jobs: [],
      error: false
    };
  }
    
  componentDidMount(){
    fetch('http://localhost/jobs', {mode: 'cors'})
    .then(res => res.json())
    .then(json => {
        this.setState({
            jobs: json,
        })
    })
    if (typeof window !== 'undefined') {

        
        document.body.className = "we-work-with bg-white";
        var s = skrollr.init();
        $(document).ready(function() {
          s.refresh();
          console.log('Loaded');
      });
    }
  }
  componentDidUpdate() {
    var s = skrollr.init();
    s.refresh();
    console.log('update');
  }
  componentWillUnmount(){
    if (typeof window !== 'undefined') {
    }
}

  render() {
    const introCont = (
      <div className="header--intro">
        <h1 className="intro--text txt_green">Statement about  <br />hiring passionate  <br /><span className="txt_black">staff at at Galloways.</span> </h1>
        <Link prefetch href="/about">
          <Button href="/about"  variant="outlined" className="btn-large darkBlue" size="large"> CONTACT US <i className="material-icons"> keyboard_arrow_right </i></Button>
        </Link>
        <div className="intro--img" ></div>
      </div>)
    const overlay = ''
    const jobs = this.state.jobs

    return (
      <Layout {...this.props} navmenu={false} container={false} content={introCont} overlay={overlay} darkMenu={'menu--dark'} topMenu={'txt_green'}>
        <section className="bg-white">
            <Container>
                <div className="vacancies">
                { jobs.map((job, index) => {
                    return (
                    <ExpansionPanel>
                        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                            <div className="vacancy">
                                <div className="vacancy--title">
                                    { job.title[0].value }
                                </div>
                                <div className="vacancy--details">
                                    <div className="vacancy--details-location">
                                        <i className="material-icons"> <img src="/static/assets/img/icons/location.png" /> </i>
                                        { job.field_location[0].value }
                                    </div>
                                    <div className="vacancy--details-salery">
                                        <i className="material-icons"> <img src="/static/assets/img/icons/money.png" />  </i>
                                        { job.field_salary[0].value }
                                    </div>
                                    <div className="vacancy--details-type">
                                        <i className="material-icons"> <img src="/static/assets/img/icons/time.png" />  </i>
                                        { job.field_contract_type[0].value }
                                    </div>
                                </div>
                            </div>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <div className="vacancy--text">
                                { renderHTML(job.field_description[0].value) }
                            </div>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                        );
                    })}
                </div>
            </Container>
        </section>
      </Layout>
    )
  }
}
