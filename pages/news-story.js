import React from 'react'
import Link from 'next/link'
import { Container, Row, Col, Jumbotron, ListGroup, ListGroupItem } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'
import '../components/parallax'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Icon from '@material-ui/core/Icon'
import $ from "jquery";
import '../components/team';
import Fade from 'react-reveal/Fade';
import Slide from 'react-reveal/Slide';
import renderHTML from 'react-render-html';

  
export default class extends Page {
  
  static getInitialProps ({ query: { id } }) {
    return { storyId: id }
  }


  
  constructor(props) {
    super(props)
    this.state = {
        news: [],
        isLoaded: false,
        storyId: this.props.storyId
    };
  }
  componentDidMount(){

    fetch('http://localhost/news', {mode: 'cors'})
    .then(res => res.json())
    .then(json => {
        this.setState({
            isLoaded: true,
            news: json,
        })
    })
    if (typeof window !== 'undefined') {

        
        document.body.className = "news bg-darkBlue";
        var s = skrollr.init();
        $(document).ready(function() {
          s.refresh();
          console.log('Loaded');
      });
    }
  }
  componentDidUpdate() {
    var s = skrollr.init();
    s.refresh();
    console.log('update = ' + this.state.storyId);
  }
  componentWillUnmount(){
    if (typeof window !== 'undefined') {
    }
}

  render() {
    const introCont = (
    <div className="header--intro">
      </div>)
    const overlay = ''
    const storyId = this.state.storyId;
    const posts = this.state.news;
    console.log(posts)
    posts.map((post, index) => {
        if(post.title[0].value.replace(/\s+/g, '-').toLowerCase() == storyId){
            const bgImage = post.field_image[0].url;
            const currentPost = index
            console.log(bgImage);
            console.log(currentPost);
        }
    })
    return (
      <Layout {...this.props} navmenu={false} container={false} content={introCont} overlay={overlay} darkMenu={'menu--darkBlue'} topMenu={'txt_green'} headerImage={'${bgImage}'}>

        <section className="bg-white">
            <Container>
                <Row className="news-story">
                    { posts.map((post, index) => {
                        if(post.title[0].value.replace(/\s+/g, '-').toLowerCase() == storyId){
                        return (
                            <Fade up>
                                <Col xs="12" md={{ size: '8', offset: 2}} key="${index}">    
                                    <h1 className="news-story--title">{ post.title[0].value }</h1>
                                    <div className="news-story--date">28 FEB 2019 / Posted by Tom Bowen  </div>
                                    <div className="news-story--content">
    
                                        { renderHTML(post.body[0].value) }
                                    </div>
                                </Col>
                            </Fade>
                        );
                    }})}
                </Row>
                <Row className="d-flex justify-content-center pt-5 pagination">
                    <a href=""><i className="material-icons"> keyboard_arrow_left </i> SEE OLDER STORIES </a>
                    <a href="">SEE NEWER STORIES <i className="material-icons"> keyboard_arrow_right </i></a>
                </Row>

            </Container>
        </section>
      </Layout>
    )
  }
}
