import React from 'react'
import Link from 'next/link'
import { Container, Row, Col, Jumbotron, ListGroup, ListGroupItem } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'
import '../components/parallax'
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Icon from '@material-ui/core/Icon'
import { $, jQuery } from "jquery";
import '../components/team';
import Fade from 'react-reveal/Fade';
import Slide from 'react-reveal/Slide';
import renderHTML from 'react-render-html';


export default class extends Page {
  constructor(props) {
    super(props)
    this.state = {
      page_data: [],
      testimonials: [],
      error: false
    };
  }

  componentWillMount(){
    fetch('http://localhost/testimonials', {mode: 'cors'})
    .then(res => res.json())
    .then(json => {
        this.setState({
          testimonials: json,
      })
    })
    fetch('http://localhost/we-work-with', {mode: 'cors'})
    .then(res => res.json())
    .then(json => {
        this.setState({
          page_data: json,
		})
		if (typeof window !== 'undefined') {
        $(document).ready(function() {
          s.refresh();
          $('.client--slider').slick({
            centerMode: true,
            slidesToShow: 1,
            centerPadding: '150px',
            arrows: true,
            prevArrow: '<button class="slick-prev slick-arrow" aria-label="Previous" type="button" style="display: block;"><i class="material-icons">chevron_left</i></button> ',
            nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button" style="display: block;"><i class="material-icons">chevron_right</i></button>',
            responsive: [
              {
                breakpoint: 768,
                settings: {
                  arrows: false,
                  centerMode: true,
                  centerPadding: '40px',
                  slidesToShow: 3
                }
              },
              {
                breakpoint: 480,
                settings: {
                  arrows: false,
                  centerMode: true,
                  centerPadding: '40px',
                  slidesToShow: 1
                }
              }
            ]
          });
          window.dispatchEvent(new Event('resize'));
	  });
	}
	})
	
    if (typeof window !== 'undefined') {

      var $ = require('jquery');
      window.jQuery = $;
      require('../node_modules/slick-carousel/slick/slick.js');


        document.body.className = "work-with bg-pink";
		var s = skrollr.init();
		$(document).ready(function() {
			s.refresh();
			$('.client--slider').slick({
			  centerMode: true,
			  slidesToShow: 1,
			  centerPadding: '150px',
			  arrows: true,
			  prevArrow: '<button class="slick-prev slick-arrow" aria-label="Previous" type="button" style="display: block;"><i class="material-icons">chevron_left</i></button> ',
			  nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button" style="display: block;"><i class="material-icons">chevron_right</i></button>',
			  responsive: [
				{
				  breakpoint: 768,
				  settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '40px',
					slidesToShow: 3
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '40px',
					slidesToShow: 1
				  }
				}
			  ]
			});
			window.dispatchEvent(new Event('resize'));
		});
    }
  }
  componentDidUpdate() {
    var s = skrollr.init();
    s.refresh();
    console.log('update');
  }
  componentWillUnmount(){
    if (typeof window !== 'undefined') {
    }
}

  render() {
    const data = this.state.page_data;
    const testimonials = this.state.testimonials;

    const introCont = (
      <div>
      { data.map((page, index) => {
        return (
          <div key={index} className="header--intro">
            <h1 className="intro--text darkBlue">{ page.field_header_text[0].value }</h1>
            <Link prefetch href="/about">
              <Button href="/about"  variant="outlined" className="btn-large darkBlue" size="large"> CONTACT US <i className="material-icons"> keyboard_arrow_right </i></Button>
            </Link>
            <div className="intro--img" ></div>
          </div>
          );
        })}
        </div>
      )
    const overlay = ''
    const { animationContainerReference } = this.props;

    return (
      <Layout {...this.props} navmenu={false} container={false} content={introCont} overlay={overlay} darkMenu={'menu--dark'}>
      { data.map((page, index) => {
        return ( <div>
      <section className="clients bg-white">
          <Container className="wide">
            <div className="row d-flex">
                <Row>
                    <Col xs="12" md={{ size: '8', offset: 2 }}>
                        <div className="clients--smallText">
                          { renderHTML(page.field_testimonial_text[0].value) }
                        </div>
                    </Col>
                </Row>
                <Row>
                    <div className="client--slider">
                    { testimonials.map((page, index) => {
                      return (
                        <div className="client">
                            <img src={ page.field_image[0].url } />

                            <div className="client--text">
                              { renderHTML(page.field_testimonial_text[0].processed) }
                            </div>
                            <div className="client--name">
                                <div className="client--name-divider"></div>
                                { page.title[0].value }
                            </div>
                        </div>
                        );
                      })}
                    </div>
                </Row>
            </div> 
         </Container>
       </section>
       <section className="bg-white clients pb-0">
          <Container>
            <Row className="d-flex justify-content-center">
              <h2>
                <div className="darkBlue">
                  Sectors we work in
                </div>
              </h2>
            </Row>
            <Row>
              <Col xs="12" md={{ size: '6', offset: 3 }}>
                <div className="clients--smallText">
                  <p>These are some of the sectors that we represented in our portfolio.</p>
                </div>
              </Col>
            </Row>
          </Container>
          <Container fluid className="sectors">
            <Row>
              <Col xs="12" md="4" className="p-0">
                <div className="sector bg-green">
                  <div className="sector--name">Hospitality and leisure</div>
                  <div className="sector--text">
                    <p>
                      Our biggest office is in Brighton.  Brighton is the UK’s no.1 leisure city.  How could we not love looking after restaurateurs, hoteliers, publicans and all the other entrepreneurs that make Brighton buzz on a Saturday night.
                    </p>
                  </div>
                </div>
              </Col>
              <Col xs="12" md="4" className="p-0">
              <div className="sector bg-orange">
                <div className="sector--name">Technology</div>
                <div className="sector--text">
                  <p>
                    Forget Silicon Valley or Silicon Glen. Try Silicon Beach. Brighton is now one of the top destinations for tech investment (cheaper than London, cooler than Cambridge….) and we have some of the best and smartest local tech companies as our clients.  
                  </p>
                </div>
              </div>
              </Col>
              <Col xs="12" md="4" className="p-0">
              <div className="sector bg-purple">
                <div className="sector--name">Property</div>
                <div className="sector--text">
                  <p>
                    Residential and commercial property investment is one of the foundations of the Brighton economy.  We have deep specialist expertise in the structuring and management of property portfolios and navigating the endless rule changes that professional landlords are faced with every year.
                  </p>
                </div>
              </div>
              </Col>
            </Row>

            <Row>
            <Col xs="12" md="4" className="p-0">
              <div className="sector bg-darkBlue">
                <div className="sector--name">Charities</div>
                <div className="sector--text">
                  <p>
                    We look after many of the most prominent Sussex charities.   It is always a challenge balancing our commercial needs with that of serving people who are looking after the most vulnerable in society. We take that challenge very seriously and are immensely proud of the charity work we do and the long-term relationships we have built with those clients
                  </p>
                </div>
              </div>
            </Col>
            <Col xs="12" md="4" className="p-0">
            <div className="sector bg-lightBlue">
              <div className="sector--name">Contractors</div>
              <div className="sector--text">
                <p>
                  To lamely paraphrase Hamlet; ‘ to be self employed or not  - that is the question’.   And it’s a question that gets harder each year as the rules keep changing.  We have a dedicated team of specialists that only look after self-employed contractors who can help you navigate the ambiguities of IR35 and help manage your cashflow.
                </p>
              </div>
            </div>
            </Col>
            <Col xs="12" md="4" className="p-0">
            <div className="sector bg-yellow">
              <div className="sector--name">Agriculture & Farming</div>
              <div className="sector--text">
                <p>
                  Sussex is a beautiful county and much of it is agricultural.  Many farms are multi-generational family enterprises and have complex needs around financing and tax.  Many of our longest standing clients have been farming Sussex for decades and we love working with them. 
                </p>
              </div>
            </div>
            </Col>
          </Row>
          </Container>
          <div className="section--overlay bg-white" data-0="width: 55%;" data-2000="width: 60%;"></div>
       </section>
       </div>
       );
      })}
      </Layout>
    )
  }
}
