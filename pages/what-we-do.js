import React from 'react'
import Link from 'next/link'
import { Container, Row, Col, Jumbotron, ListGroup, ListGroupItem } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'
import '../components/parallax'
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Icon from '@material-ui/core/Icon'
import $ from "jquery";
import '../components/team';
import Fade from 'react-reveal/Fade';
import Slide from 'react-reveal/Slide';
import renderHTML from 'react-render-html';


export default class extends Page {
  constructor(props) {
    super(props)
    this.state = {
      page_data: [],
      error: false
    };
  }

  componentWillMount(){
    fetch('http://localhost/what-we-do', {mode: 'cors'})
    .then(res => res.json())
    .then(json => {
        this.setState({
          page_data: json,
        })
    })
    if (typeof window !== 'undefined') {
        document.body.className = "what-we-do bg-lightBlue";
        var s = skrollr.init();
        $(document).ready(function() {
          s.refresh();
          console.log('Loaded');
      });
    }
  }
  componentDidUpdate() {
    var s = skrollr.init();
    s.refresh();
    console.log('update');
  }
  componentWillUnmount(){
    if (typeof window !== 'undefined') {
    }
}

  render() {
    const overlay = ''
    const { animationContainerReference } = this.props;
    const data = this.state.page_data;
    const introCont = (
      <div className="header--intro">
      { data.map((page, index) => {
        return (<div>
        <h1 className="intro--text txt_green">{ page.field_header_text[0].value }</h1>
        <Link prefetch href="/about">
          <Button href="/contact"  variant="outlined" className="btn-large darkBlue" size="large"> CONTACT US <i className="material-icons"> keyboard_arrow_right </i></Button>
        </Link>
        <div className="intro--img" ></div>
        </div>);
        })}
      </div>)

    return (
      <Layout {...this.props} navmenu={false} container={false} content={introCont} overlay={overlay} darkMenu={'menu--dark'} topMenu={'txt_green'}>
      { data.map((page, index) => {
        return (
          <div>
      <section className="bg-darkBlue">
          <Container className="wide">
            <div className="row d-flex">
              <Col xs="12" md="4" lg="4" className="align-self-center txt_white">
                <Fade up>
                <h2 className="txt_green">
                    { page.field_section1_title[0].value }
                </h2>
                { renderHTML(page.field_section1_text[0].processed) }
                
                  <Button href="/"  variant="outlined" className="btn-large darkBlue" size="large"> READ MORE <i className="material-icons"> keyboard_arrow_right </i></Button>
                </Fade>
              </Col>
              <Col xs="12" md="7" lg={{ size: "7", offset: 1 }} className="d-flex justify-content-end" ref={animationContainerReference}>
                <Fade right>
                  <img src={page.field_section_1_image[0].url} />
                </Fade>
              </Col>
            </div> 
         </Container>
       </section>
       <section className="bg-white">
          <Container>
            <div className="section services">
              <Row className="d-flex justify-content-center">
                <h2>
                  <div className="darkBlue">
                    Business services
                  </div>
                </h2>
              </Row>
              <Row>
                <Col xs="12" lg="6" md={{ size: '6' }}>
                  <div className="services--icon">
                    <img src="/static/assets/img/home/service1.png" />
                  </div>
                  <div className="services--title">
                    Statutory accounting and audit
                  </div>
                  <div className="services--text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
                  </div>
                </Col>
                <Col xs="12" lg="6" md={{ size: '6' }}>
                  <div className="services--icon">
                    <img src="/static/assets/img/home/service1.png" />
                  </div>
                  <div className="services--title">
                    Management accounting and bookkeeping
                  </div>
                  <div className="services--text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
                  </div>
                </Col>
              </Row>
              <Row>
                <Col xs="12" lg="6" md={{ size: '6' }}>
                  <div className="services--icon">
                    <img src="/static/assets/img/home/service1.png" />
                  </div>
                  <div className="services--title">
                    Management accounting and bookkeeping
                  </div>
                  <div className="services--text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
                  </div>
                </Col>
                <Col xs="12" lg="6" md={{ size: '6' }}>
                  <div className="services--icon">
                    <img src="/static/assets/img/home/service1.png" />
                  </div>
                  <div className="services--title">
                    Payroll
                  </div>
                  <div className="services--text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
                  </div>
                </Col>
              </Row>
              <Row>
              <Col xs="12" lg="6" md={{ size: '6' }}>
                <div className="services--icon">
                  <img src="/static/assets/img/home/service1.png" />
                </div>
                <div className="services--title">
                  Corporation tax compliance and annual returns
                </div>
                <div className="services--text">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
                </div>
              </Col>
              <Col xs="12" lg="6" md={{ size: '6' }}>
                <div className="services--icon">
                  <img src="/static/assets/img/home/service1.png" />
                </div>
                <div className="services--title">
                  Vat compliance and planning
                </div>
                <div className="services--text">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
                </div>
              </Col>
            </Row>
            <Row>
            <Col xs="12" lg="6" md={{ size: '6' }}>
              <div className="services--icon">
                <img src="/static/assets/img/home/service1.png" />
              </div>
              <div className="services--title">
                Specialist tax planning services
              </div>
              <div className="services--text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
              </div>
            </Col>
            <Col xs="12" lg="6" md={{ size: '6' }}>
              <div className="services--icon">
                <img src="/static/assets/img/home/service1.png" />
              </div>
              <div className="services--title">
                Business improvement advice
              </div>
              <div className="services--text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
              </div>
            </Col>
          </Row>
            </div>
          </Container>
          <div className="section--overlay bg-white" data-0="width: 55%;" data-2000="width: 60%;"></div>
       </section>
       <section className="bg-green">
        <Container>
          <div className="section services bg-green txt_white">
            <Row className="d-flex justify-content-center">
              <h2>
                <div className="txt_white">
                  Business services
                </div>
              </h2>
            </Row>
            <Row>
              <Col xs="12" lg="6" md={{ size: '6' }}>
                <div className="services--icon">
                  <img src="/static/assets/img/home/service2.png" />
                </div>
                <div className="services--title">
                  Annual tax compliance
                </div>
                <div className="services--text">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
                </div>
              </Col>
              <Col xs="12" lg="6" md={{ size: '6' }}>
                <div className="services--icon">
                  <img src="/static/assets/img/home/service2.png" />
                </div>
                <div className="services--title">
                  Specialist tax planning services
                </div>
                <div className="services--text">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
                </div>
              </Col>
            </Row>
            <Row>
              <Col xs="12" lg="6" md={{ size: '6' }}>
                <div className="services--icon">
                  <img src="/static/assets/img/home/service2.png" />
                </div>
                <div className="services--title">
                  Probate
                </div>
                <div className="services--text">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
                </div>
              </Col>
              <Col xs="12" lg="6" md={{ size: '6' }}>
                <div className="services--icon">
                  <img src="/static/assets/img/home/service2.png" />
                </div>
                <div className="services--title">
                  Financial planning and wealthmanagement advice
                </div>
                <div className="services--text">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
                </div>
              </Col>
            </Row>
          </div>
        </Container>
      </section>
       <section className="bg-lightBlue vacancies-footer">
        <Container>
          <div className="row d-flex">
            
            <Col xs="12" md={{ size: '6', offset: '1  ' }} lg="6">
            </Col>
            <Col xs="12" md="4" lg="4" className="align-self-center txt_white">
              <h2>
                <div>
                  We’re hiring
                </div>
              </h2>
              <p>
                We are always looking for great people, at all levels from partner to trainee, to join our team.
              </p>
              <Link prefetch href="/about">
                <Button href="/"  variant="outlined" className="btn-large darkBlue" size="large"> VIEW CURRENT VACANCIES <i className="material-icons"> keyboard_arrow_right </i></Button>
              </Link>
            </Col>
          </div>
        </Container>
       </section>
       </div>
       );
      })}
      </Layout>
    )
  }
}
